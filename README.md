## Amex coding assignment

# JL Notes:
I saw the 1st assignment for Kotlin example and discussed with recruiter who sent
modified version for those who speak other langauges.  This assignment is in repo.
After some debate - I decided to write in Golang - for fallowing reason's
-To create the rest endpoints - with Spring/Kotlin/Gradle I concluded I might get
caught up in some Spring/Boot issues.
-GoLang - has all the basic parts built in to do the task.

# Requirements
- GoLang 1.15+
- Connection to internet

# Running
#go run main.go

# Sending data
`curl -H "Contect-Type: application/json" -X POST -d '{"apple":1,"orange":2}' http://localhost:8080`


#Step 2 Notes:
If this was not exercise - I'd seek business owner and clarify exact rules for offer.  Are the offer's independant of each other?  or can be combined?

For our purposes - I'll assume they can be combined and don't have limit .. 
If they order 3 Apples - they'll pay for 3 and get 6 .. if they order 4 Oranges - they'll pay for 4 and get 6



Step 3 Notes:
Used GoLang - Bigcache - sort of a native Redis Golang implementation - works out of box
without needing to start service.  Currently this cache is in memory BUT could be written to disc
if so configured.  BigCache can also be distributed cache - if there are more then one worker
* Chosen - easy Go implementation
+ Can Scale/Grow if need

load some data - running this a few times
`curl -X POST --data @./order.json http://localhost:8080`

# Get ALL offers
`curl -X POST http://localhost:8080/getoffers`

# Get a particular offer - Note records are stored in
order they go in
`curl -X POST http://localhost:8080/getoffer?recordnum=1`


# .. Testing NOTE
Individual tests - work but with cache - likely somekind of test runner trying mult things
at same time.

Run the testsuite - in order
``go test -timeout 30s -run ^TestRun$ gitlab/amextest``
 - to run tests.... 
`go test -timeout 30s -run ^TestGetOrder$ gitlab/amextest\`
`go test -timeout 30s -run ^TestGetOrders$ gitlab/amextest\`
`go test -timeout 30s -run ^TestGetResource$ gitlab/amextest\`
`go test -timeout 30s -run ^TestGetOffer$ gitlab/amextest`\
