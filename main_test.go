package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

//TestGetResource - a test go resources from database
func TestGetResource(t *testing.T) {
	cache.Reset()
	var jsonData = []byte(`{"Apple":3, "Orange":3}`)

	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(orderPost)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"Cost":"2.55","Order":{"Apple":3,"Orange":3}}`

	require.JSONEq(t, expected, rr.Body.String())
	cache.Reset()
}
func TestOrderOfferPost(t *testing.T) {
	cache.Reset()
	var jsonData = []byte(`{"Apple":1, "Orange":2}`)

	req, err := http.NewRequest("POST", "/offer", bytes.NewBuffer(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(orderofferPost)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"Cost":"1.10","Order":{"Apple":2,"Orange":3}}`

	require.JSONEq(t, expected, rr.Body.String())
	cache.Reset()
}

func TestGetOrder(t *testing.T) {
	// Clear the cache - entries from other tests - may throw off
	cache.Reset()

	var jsonData1 = []byte(`{"Apple":1, "Orange":2}`)
	runreq(jsonData1, t)

	expected := `[{"Cost":"1.10","Order":{"Apple":1,"Orange":2}}]`

	// Now - pull the saved record
	req2, err2 := http.NewRequest("GET", "/getoffer?recordnum=1", nil)
	if err2 != nil {
		t.Fatal(err2)
	}
	req2.Header.Set("Content-Type", "application/json")
	rr2 := httptest.NewRecorder()
	handler2 := http.HandlerFunc(getorder)
	handler2.ServeHTTP(rr2, req2)

	// if status := rr2.Code; status != http.StatusOK {
	// 	t.Errorf("handler returned wrong status code: got %v want %v",
	// 		status, http.StatusOK)
	// }

	require.JSONEq(t, expected, rr2.Body.String())
	cache.Reset()
}

func TestGetOrders(t *testing.T) {
	// Crerate Request - to fill in
	// Clear the cache - entries from other tests - may throw off
	cache.Reset()

	var jsonData1 = []byte(`{"Apple":1, "Orange":2}`)
	var jsonData2 = []byte(`{"Apple":2, "Orange":3}`)
	runreq(jsonData1, t)
	runreq(jsonData2, t)

	expected := `[{"Cost":"1.10","Order":{"Apple":1,"Orange":2}},{"Cost":"1.95","Order":{"Apple":2,"Orange":3}}]`

	// Now - pull the saved record
	req2, err2 := http.NewRequest("GET", "/getoffers", nil)
	if err2 != nil {
		t.Fatal(err2)
	}
	req2.Header.Set("Content-Type", "application/json")
	rr2 := httptest.NewRecorder()
	handler2 := http.HandlerFunc(getorders)
	handler2.ServeHTTP(rr2, req2)

	if status := rr2.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	require.JSONEq(t, expected, rr2.Body.String())
	cache.Reset()

}
func runreq(data []byte, t *testing.T) {
	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(orderPost)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestRun(t *testing.T) {
	t.Run("group", func(t *testing.T) {
		t.Run("Test1", TestGetOrders)
		t.Run("Test1", TestGetOrders)
		t.Run("Test3", TestGetResource)
		t.Run("Test4", TestOrderOfferPost)
	})
	// <tear-down code>
}
