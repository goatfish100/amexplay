package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync/atomic"
	"time"

	"github.com/allegro/bigcache"
)

type count32 int32

func (c *count32) inc() int32 {
	return atomic.AddInt32((*int32)(c), 1)
}

func (c *count32) get() int32 {
	return atomic.LoadInt32((*int32)(c))
}

var CId count32

type Order struct {
	Apple  int `json:"Apple"`
	Orange int `json:"Orange"`
}

type OrderSummary struct {
	Cost  string `json:"Cost"`
	Order Order
}
type Orders []OrderSummary

const (
	AppleCost  = 60
	OrangeCost = 25
)

var cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(180 * time.Minute))

// take a value in cents - return dallars and cents
func toDallar(conv int) string {
	return strconv.Itoa(conv/100) + "." + strconv.Itoa(conv%100)
}

func orderPost(rw http.ResponseWriter, request *http.Request) {
	t := Order{}
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&t) // err := decoder.Decode(&str)

	if err != nil {
		panic(err)
	}
	log.Println("Apple Orange ", t.Orange, t.Apple)
	result := t.Apple*AppleCost + t.Orange*OrangeCost
	//Get Calculated cost - don't worry about currency stuff at moment -- KISS

	var cost = toDallar(result)

	// Put in json
	ordersummary := OrderSummary{
		Cost:  cost,
		Order: t,
	}
	CId.inc()
	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(ordersummary)

	cache.Set(strconv.Itoa(int(CId.get())), reqBodyBytes.Bytes())

	log.Println("Cost is ", cost)

	//Set the Headers
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(rw).Encode(ordersummary); err != nil {
		log.Println("GetResource error encoding json", err)
		panic(err)
	}

}

// buy one - get one for Apples
func orderofferPost(rw http.ResponseWriter, request *http.Request) {
	t := Order{}
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&t) // err := decoder.Decode(&str)

	if err != nil {
		panic(err)
	}
	log.Println("Apple Orange ", t.Orange, t.Apple)

	result := t.Apple*AppleCost + t.Orange*OrangeCost
	if t.Apple >= 1 {
		t.Apple = t.Apple * 2
	}
	if t.Orange%2 == 0 {
		t.Orange = t.Orange/2 + t.Orange
	}
	//Get Calculated cost - don't worry about currency stuff at moment -- KISS

	var cost = toDallar(result)

	// Put in json
	ordersummary := OrderSummary{
		Cost:  cost,
		Order: t,
	}
	CId.inc()
	//Store in cache
	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(ordersummary)
	cache.Set(strconv.Itoa(int(CId.get())), reqBodyBytes.Bytes())
	log.Println("Cost is ", cost)

	//Set the Headers
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(rw).Encode(ordersummary); err != nil {
		log.Println("GetResource error encoding json", err)
		panic(err)
	}

}

// buy one - get one for Apples
func getorders(rw http.ResponseWriter, request *http.Request) {

	t := OrderSummary{}
	ts := []OrderSummary{}
	// var orders []OrderSummary
	x := CId.get()
	log.Println(x)

	for i := 1; i <= int(x); i++ {
		entry, err := cache.Get(strconv.Itoa(i))
		if err == nil {
			err := json.Unmarshal([]byte(entry), &t)
			if err != nil {
				log.Println("error unmarshalling ", err)
			}
			log.Println(t.Cost)
			ts = append(ts, t)
		}

	}

	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(rw).Encode(ts); err != nil {
		log.Println("GetResource error encoding json", err)
		panic(err)
	}

}

// buy one - get one for Apples
func getorder(rw http.ResponseWriter, request *http.Request) {
	recordnum := request.URL.Query().Get("recordnum")
	id, err := strconv.Atoi(recordnum)
	if err != nil {
		panic(err)
	}
	log.Println("record number", recordnum)

	t := OrderSummary{}
	ts := []OrderSummary{}
	log.Println(recordnum)
	entry, err := cache.Get(strconv.Itoa(id))
	log.Println(entry)
	if err == nil {
		err := json.Unmarshal([]byte(entry), &t)
		if err != nil {
			log.Println("error unmarshalling ", err)
		}
		log.Println("getorder cost", t.Cost)
		ts = append(ts, t)

	} else {
		rw.WriteHeader(400)
	}

	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(rw).Encode(ts); err != nil {
		log.Println("GetResource error encoding json", err)
		panic(err)
	}

}

func handleRequests() {

	// Add some good practices
	port := os.Getenv("PORT")
	if "" == port {
		port = ":8080"
	}
	log.Println("port running on ", port)
	http.HandleFunc("/", orderPost)
	http.HandleFunc("/offer", orderofferPost)
	http.HandleFunc("/getoffers", getorders)
	http.HandleFunc("/getoffer", getorder)
	log.Fatal(http.ListenAndServe(port, nil))
}

func main() {
	handleRequests()
}
